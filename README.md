# Text for Our Revolution Shift Scheduler

This is a ruby / padrino web application that allows users to authenticate with slack and sign up for shifts to text for bernie.

If you're interested in getting started *as a texter* please visit http://textforbernie.com

Developers, please read on!

## Development

This is a [padrino](http://padrinorb.com/)-based application that uses [sequel](http://sequel.jeremyevans.net/) ORM for database management and [slim](http://slim-lang.com/) for views.

### Prerequisites

* git
* ruby 2.2.1 ([rvm](https://rvm.io/) preferred)
* mysql

If you would prefer to use sqlite or another database for development purposes, simply edit `config/database.rb` and ensure that your environment variables are properly set up.

### Setup

* Clone the repository `git clone git@bitbucket.org:jamina1/t4b-shifts.git`
* Install dependencies `bundle install`
* Customize the Environment variables (see `.env.example` for required vars)
* Rename .env file for stages (development, production, etc.)
* Create and migrate the database `padrino rake sq:create sq:migrate`
* Run `padrino start`

### Testing

* Run `padrino rake test` to run tests (minitest)

### Deployment

This repo is capistrano-ready. Ensure to update your deployment configuration in `config/deploy.rb`

# Contributing

## Making changes

In order to make changes, make your changes on a fork of the this repository and then issue a pull request. Please follow the advice below to ensure that your contribution will be readily accepted.

### Make your changes

* Create a branch for your changes (optional, but highly encouraged)
* Please test your changes (this site is mobile responsive so please make sure that any edits are compatible with multiple browsers and devices)
* Push your changes up to your fork at github

### Make a pull request

When you have finished making your changes and testing them, go to your forked repo on github and issue a pull request for the branch containing your changes.  Please keep in mind the following before submitting your pull request:

* We favor pull requests with very small, single commits with a single purpose.  If you have many changes, they'll be more readily received as separate pull requests