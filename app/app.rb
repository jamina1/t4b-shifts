module Textshifts
	class App < Padrino::Application
		register ScssInitializer
		register Padrino::Mailer
		register Padrino::Helpers

		helpers Sinatra::Cookies

		require 'json'
		require 'oj'

		configure :development do
			disable :asset_stamp # no asset timestamping for dev
			OmniAuth.config.on_failure = Proc.new { |env|
				OmniAuth::FailureEndpoint.new(env).redirect_to_failure
			}
		end

		configure :test do
			set :show_exceptions, true
		    set :logging, true
		end

		before do
			if current_user && (current_user.phone.blank? || current_user.email.blank? || current_user.tz.blank?)
				flash.now[:danger] = "You can't sign up for shifts without completing your profile! <a href='#{url_for(:users, :edit)}' class='alert-link'>Click here</a> to finish setting up your profile!"
			end

			if current_user && !(current_user.active?)
				halt 403, :danger => "Your account has been deactivated, so you won't be able to sign up for shifts. Please contact a data manager in the #assignments channel for more information."
			end
		end

		get "/" do
			slim :index
		end

		get :login, :map => "/login" do
			redirect url_for(:shifts, :index), :info => "You're already logged in!" if current_user
			redirect url_for("/auth/slack")
		end

		error 404 do
			render 'errors/404'
		end

		error 403 do
			render 'errors/403'
		end

		error 500 do
			render 'errors/500'
		end
	end

	class Sequel::Model
		alias_method :save!, :save
	end
end
