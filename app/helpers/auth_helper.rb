# Helper methods defined here can be accessed in any controller or view in the application

module Textshifts
  class App
    module AuthHelper
    	def current_user=(user)
			@current_user = user
		end

		def current_user?(user)
			@current_user == user
		end

		def current_user
			if !cookies["current_user"].blank? && session[:current_user].blank?
				userFromCookie = User.first_by_id(cookies["current_user"])
				sign_in(userFromCookie)
			else
				@current_user ||= User.first_by_id(session[:current_user])
			end
		end

		def sign_in(user)
			# Set session
			session[:current_user] = user.id
			session[:roles] = user.roles

			# Save cookie
			cookies[:current_user] = session[:current_user]

			# update_last_logged_in_time(user)
			self.current_user = user
			@current_user = user
		end

		def sign_out
			cookies.clear
			session.clear
		end

		def logged_in?
			!current_user.blank?
		end

		def authorized?(allowed_roles)
			return false if session[:current_user].blank?
			halt 403, error: "Your account has been deactivated." unless current_user.active?
			return true unless allowed_roles
			return false unless session[:roles]
			roles = session[:roles]
			roles.any? { |r| allowed_roles.include? r }
		end

		def authorize!(allowed_roles=nil)
			if logged_in?
				halt 403 unless authorized?(allowed_roles)
			else
				redirect url_for("/login?return_url=#{request.env['PATH_INFO']}"), info: "You need to be logged in to access this." unless authorized?(allowed_roles)
			end
		end

		def in_channel(token, channel = ENV['TEXT_TEAM_ID'], team = ENV['SLACK_TEAM_ID'])
			require 'httparty'
			response = HTTParty.get("https://slack.com/api/groups.list?token=#{token}&exclude_archived=1&team=#{team}")
			return !!response["groups"].find {|h1| h1["id"] == channel}
		end

    end

    helpers AuthHelper
  end
end
