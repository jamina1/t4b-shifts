# Helper methods defined here can be accessed in any controller or view in the application

module Textshifts
	class App
	module AssignmentHelper
		def status_class(status)
			case status
				when :claimed, 1
					"warning"
				when :assigned, 2
					"success"
				when :cancel, 3
					"active"
				when :missing, 4
					"danger"
			end
		end

		def statusNum(status)
			case status
				when "pending", :pending
					0
				when "claimed", :claimed
					1
				when "assigned", :assigned
					2
				when "cancel", :cancel
					3	
				when "missing", :missing
					4
			end
		end

		def notifyUsers(users, message)
			require 'httparty'
			@at = users.map {|u| "@" + u }.join(" ")
			message = "Hey!! You've been assigned to #{params[:assigned_to]}! Make sure to check out the <http://bernie.to/t4binfo|Info Doc> for assignment and event details! Get texting and good luck!" if message.blank?
			assigned_by = "Contact @#{current_user.uid} with questions!"
			requestParams = {:text => "#{@at}\n#{message} #{assigned_by}", :link_names => 1}
			requestParams[:channel] = "@oranges13" unless Padrino.env == :production
			response = HTTParty.post(ENV['SLACK_MESSAGE_URL'],
				:body => requestParams.to_json
			)
		end

		def self.hollerAnnounce(message)
			require 'httparty'
			requestParams = {:text => "#{message}", :link_names => 1}
			requestParams[:channel] = "@oranges13" unless Padrino.env == :production
			response = HTTParty.post(ENV['SLACK_MESSAGE_URL'],
				:body => requestParams.to_json
			)
		end

		def self.newbieAnnounce(message)
			require 'httparty'
			requestParams = {:text => "#{message}", :link_names => 1, :username => "Birdie Sanders"}
			requestParams[:channel] = "@oranges13" unless Padrino.env == :production
			response = HTTParty.post(ENV['WELCOME_MESSAGE_URL'],
				:body => requestParams.to_json
			)
		end
	end

	helpers AssignmentHelper
	end
end
