# Helper methods defined here can be accessed in any controller or view in the application

module Textshifts
  class App
	module ShiftHelper
		def markdown(text)
			options = {
				filter_html: true,
				hard_wrap: true, 
				link_attributes: { rel: 'nofollow', target: "_blank" },
				space_after_headers: true, 
				fenced_code_blocks: true
			}

			extensions = {
				autolink: true,
				superscript: true,
				disable_indented_code_blocks: true
			}

			renderer = Redcarpet::Render::HTML.new(options)
			markdown = Redcarpet::Markdown.new(renderer, extensions)

			markdown.render(text).html_safe
		end
	end

	helpers ShiftHelper
  end
end
