Textshifts::App.controllers :users do

  helpers Gravatarify::Helper
  
  before :view, :edit, :update, :destroy do
    authorize!()
    @user = User[current_user.id]
  end

  before :index, :admin_edit, :adminify, :activate do
    authorize!(["admin"])
  end

  get :index, :map => "/users" do
    content_for(:assets) do
      stylesheet_link_tag "https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/css/dataTables.bootstrap.min.css"
    end
    content_for(:scripts) do
      javascript_include_tag "https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/jquery.dataTables.min.js", "https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.11/js/dataTables.bootstrap.min.js"
    end
    @users = User.all
    slim :'user/index'
  end

  get :view, :map => "/profile" do
    slim :'user/view'
  end

  get :edit, :map => "/profile/edit" do
    slim :'user/edit'
  end

  put :update, :map => "/profile/update", :params => [:email, :name, :phone, :tz, :location] do
    if @user.update(params)
      redirect url_for(:users, :view), info: "Updated your user account!"
    else
      redirect url_for(:users, :edit), error: "Couldn't update your user account: #{@user.errors.full_messages.join(', ').humanize}"
    end
  end

  delete :destroy do

  end

  # Admin functions

  get :admin_edit, :with => :id do
    @user = User[params[:id]]
    halt 400 unless request.xhr?
    slim :'user/admin_edit', :layout => false
  end

  post :admin_update, :with => :id do
    halt 403 unless current_user.is_admin?
    halt 400 unless request.xhr?
    @user = User[params[:id]]
    !!@user.update(params[:user])
  end

  post :adminify, :with => :id do
    halt 403 unless current_user.is_admin?
    halt 400 unless request.xhr?
    @user = User[params[:id]]
    roles = @user.roles || []
    if roles.include?("admin")
      roles.delete("admin")
    else
      roles << "admin"
    end
    @user.roles = roles
    !!@user.save
  end

  post :activate, :with => :id do
    halt 403 unless current_user.is_admin?
    halt 400 unless request.xhr?
    @user = User[params[:id]]
    !!@user.update(:active => !@user.active?)
  end

  private

  # Ajax load a row to replace in the table
  post :get_row, :with => :id do
    halt 400 unless request.xhr?
    @user = User[params[:id]]
    slim :'user/_admin_user_row', :layout => false, :locals => {:u => @user}
  end

end
