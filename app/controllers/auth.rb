Textshifts::App.controllers :auth do

	get :slack_callback, :map => "/auth/slack/callback" do
		omniauth = request.env["omniauth.auth"]

		# Redirect and callback again with more permissive token for new users
		redirect url_for("/auth/new_user") unless User.first_by_uid(omniauth[:info][:user])

		process_user(omniauth)

	end

	get :slack_callback, :map => "/auth/new_user/callback" do
		omniauth = request.env["omniauth.auth"]

		process_user(omniauth)

	end

	get :slack_callback_failed, :map => "/auth/failure" do
		flash[:error] = "Error logging in with Slack: #{params[:message].humanize}"
		redirect url("/")
	end

	get :destroy, :map => '/logout' do
		sign_out
		redirect url_for('/'), info: "Logged Out"
	end

	private

	define_method :process_user do |omniauth|
		if in_channel(omniauth[:credentials][:token], ENV['TEXT_TEAM_ID'], ENV['SLACK_TEAM_ID'])

			user = User.first_by_uid(omniauth[:info][:user])

			if !user.blank?
				if user.active?
					sign_in(user)
					redirect url_for('/shifts'), success: "You are logged in as #{user.uid}"
				else
					redirect url_for('/logout'), error: "Your account has been deactivated, please contact an administrator."
				end
			else
				user = User.new_from_auth(omniauth[:info])
				if user.errors.key?(:email)
					redirect url_for('/'), error: "Your email already exists under a different slack ID (#{user.uid}). Did you change your slack name? You won't be able to log in until you change it back."
				else
					redirect url_for('/'), error: "Could not log you in for the following reasons: #{user.errors.full_messages.join(', ').humanize}"
				end
			end
		else
			redirect url_for("/"), error: "You must be a member of the #assignments channel before you can log in to request shifts! Make sure that you have <a href='http://bernie.to/t4b_New_User_FAQ' class='alert-link'>read our FAQ</a> and completed our onboarding process first!"
		end
	end

end
