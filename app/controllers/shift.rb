Textshifts::App.controllers :shifts do

	before :index, :view, :signup, :signup_form, :forfeit do
		authorize!()
	end

	before :forfeit do
		redirect url_for(:shifts, :index) unless request.xhr?
		halt 400 unless @shift = Shift[params[:id]]
		halt 400 unless current_user
	end

	before :new, :create, :edit, :update, :destroy, :assign do
		authorize!(["admin"])
	end

	get :index do
		@mine = Shift.mine(current_user).upcoming.all
		@upcoming = Shift.upcoming.exclude(:users => current_user).order(:start_date).all
		slim :'shift/index'
	end

	get :feed, :provides => [:rss] do
		@shifts = Shift.upcoming.order(:start_date).all
		render :'shift/feed'
	end

	get :view, :with => :id do
		@shift = Shift[params[:id]]
		@assignment_url = url_for(:assignment, :list, @shift.id)
		@my_shifts = @shift.assignments_dataset.where(:user_id => current_user.id)
		@count_all_shifts = @shift.assignments.count
		@assigned_shifts = @shift.assignments_dataset.exclude(:status => 0).count
		slim :'shift/view'
	end

	get :signup, :with => [:id] do
		halt 400 unless request.xhr?
		@shift = Shift[params[:id]]
		slim :'shift/signup', :layout => false
	end

	post :signup, :with => [:id] do
		halt 400 unless @shift = Shift[params[:id]]
		halt 400 unless current_user
		redirect url_for(:shifts, :index), :error => "Couldn't sign you up, that shift is full!" if @shift.full?
		@amount = (params[:amount])? params[:amount].to_i : 1
		@amount.times do
			@shift.add_assignment(:user => current_user, :status => :pending, :availability => params[:availability])
		end
		redirect url_for(:shifts, :view, @shift.id), :success => "Signed you up for this Shift!"
	end

	get :forfeit, :with => [:id] do
		!!@shift.remove_user(current_user) # Cast as boolean response for Ajaxyness!
	end

	get :new do
		@form = session[:form] || Shift.new
		session.delete(:form)
		slim :'shift/new'
	end

	post :create do
		session.delete(:form) # Old info
		params[:form].delete_if {|k,v| v.blank?}
		@shift = Shift.new(params[:form])
		if @shift.save
			redirect url_for(:shifts, :view, @shift.id), :info => "Created shift!"
		else
			session[:form] = params[:form]
			redirect url_for(:shifts, :new), :error => "There was a problem creating the shift: #{@shift.errors.full_messages.join(', ').humanize}"
		end
	end

	get :edit, :with => :id do
		@shift = session[:form] || Shift[params[:id]]
		session.delete(:form)
		slim :'shift/edit'
	end

	put :update, :with => :id do
		@shift = Shift[params[:id]]
		if @shift.update(params[:form])
			redirect url_for(:shifts, :view, @shift.id), :info => "Updated shift!"
		else
			session[:form] = params[:form]
			redirect url_for(:shifts, :edit, @shift.id), :error => "There was a problem updating the shift: #{@shift.errors.full_messages.join(', ').humanize}"
		end
	end

	delete :destroy, :with => :id do
		halt 400 unless @shift = Shift[params[:id]]
		if request.xhr?
			!!@shift.destroy # Cast as boolean response for Ajaxyness!
		else
			if @shift.destroy
				redirect url_for(:shifts, :index), :info => "Deleted Shift!"
			else
				redirect url_for(:shifts, :view, @shift.id), :error => "We couldn't delete this shift: #{@shift.errors.full_messages.join(', ').humanize}"
			end
		end
	end

end
