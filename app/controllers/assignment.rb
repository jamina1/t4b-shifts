Textshifts::App.controllers :assignment do

	Oj.default_options = {:mode => :compat, :symbol_keys => false, :time_format => :ruby }
	
	before :confirm_form, :confirm, :update do
		authorize!(["admin"])
	end

	before :list, :forfeit do
		authorize!()
	end

	before :forfeit do
		halt 400 unless request.xhr?
		halt 400 unless @assignment = Assignment[params[:id]]
	end

	get :forfeit, :with => [:id] do
		halt 400 unless !!@assignment.destroy
	end

	post :list, :parent => :shift, :csrf_protection => false do
		content_type :json
		if params[:type].to_i == 1
			@a = Assignment.association_join(:user).select(:assignments__id, :assignments__created_at, :assignments__status, :assignments__assigned_to, :assignments__assigned_by, :assignments__availability, :user__uid, :user__name, :user__email, :user__tz, :user__phone, :user__location).where(shift_id: params[:shift_id]).order(:assignments__created_at).distinct(:user__uid).all
		else
			@id = Assignment.association_join(:user).where(shift_id: params[:shift_id]).order(Sequel.lit("assignments.created_at")).distinct(:uid).select_map(Sequel.lit("assignments.id"))
			@a = Assignment.association_join(:user).exclude(Sequel.lit("assignments.id") => @id).select(Sequel.lit("assignments.id"), Sequel.lit("assignments.created_at"), :status, :assigned_to, :assigned_by, :availability, :uid, :name, :email, :tz, :phone, :location).where(shift_id: params[:shift_id]).order(Sequel.lit("assignments.created_at")).all
		end
		Oj.dump(@a) 
	end

	post :confirm_form do
		halt 400 unless request.xhr?
		@a = Assignment.where(Sequel.lit("assignments.id") => params[:ids]).association_join(:user).select(Sequel.lit("assignments.id"), Sequel.lit("assignments.created_at"), :status, :assigned_to, :assigned_by, :availability, :uid, :name, :email, :tz, :phone, :location)
		slim :'assignment/confirm', :layout => false
	end
	
	post :confirm do
		halt 400 unless request.xhr?
		halt 400 unless params[:ids]
		halt 400 unless params[:assigned_to]
		@status = statusNum(:assigned)
		@a = Assignment.where(:id => params[:ids])
		@users = @a.map {|u| u.user.uid }
		if @a.update(:status => @status, :assigned_by => current_user.uid, :assigned_to => params[:assigned_to])
			if params[:notify]
				notifyUsers(@users, params[:message])
			end
			return true
		else
			return false
		end
	end

	post :update do
		halt 400 unless request.xhr?
		halt 400 unless params[:ids]
		halt 400 unless params[:status]
		@status = statusNum(params[:status])
		if @status == 0
			!!Assignment.where(:id => params[:ids]).update(:status => @status, :assigned_by => nil, :assigned_to => nil)
		else
			!!Assignment.where(:id => params[:ids]).update(:status => @status, :assigned_by => current_user.uid)
		end
	end

end
