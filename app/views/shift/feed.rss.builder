xml.instruct!
xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
    xml.title "Text for Our Revolution Shifts"
    xml.description "Available Shifts for Our Revolution"
    xml.link "#{absolute_url(:shifts, :index)}"

    @shifts.each do |shift|
      xml.item do
        xml.title shift.name.encode(:xml => :text)
        if !shift.details.blank?
          xml.description shift.details.encode(:xml => :text)
        else
          xml.description "Sign up now!"
        end
        xml.pubDate shift.start_date.iso8601()
        xml.link "#{absolute_url(:shifts, :view, :id => shift.id)}"
      end
    end
  end
end