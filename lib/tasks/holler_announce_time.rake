namespace :holler do
  
  desc 'Announces the time to start texting in a zone'
  task :announce_morning, [:zone] => :environment do |t, args|
    args.with_defaults(:zone => 'Test')

    @message = "It is now 9AM in the *#{args[:zone].upcase}* Time Zone. You may now begin sending and reply to messages in the *#{args[:zone].upcase}* Time Zone"

    Textshifts::App::AssignmentHelper::hollerAnnounce(@message)
  end

  desc 'Announces the time to stop texting for a zone'
  task :announce_evening, [:zone] => :environment do |t, args|
    args.with_defaults(:zone => 'Test')

    @message = "It is now 9PM in the *#{args[:zone].upcase}* Time Zone. Please stop sending messages to recipients in the *#{args[:zone].upcase}* Time Zone. If you receive a reply you will need to respond after 9am local time. If you're unsure of the time zone in a specific location you can google for _Time in location_. Don't get the revolution in trouble!"

    Textshifts::App::AssignmentHelper::hollerAnnounce(@message)
  end

  desc 'New Hustle Announcement'
  task :new_hustle => :environment do 
    @message = "​*Hi all! We're working on some exciting new updates at the moment, so assignments and onboarding are on hold. Hang tight and we'll have announcements as soon as they're ready!*​"

    Textshifts::App::AssignmentHelper::hollerAnnounce(@message)
  end
end

