namespace :textforbernie_new do

  desc 'Regularly posts the link to the FAQ document in the newbie channel'
  task :welcome => :environment do |t, args|
    @document = "http://trello.com/b/DYyA3uPR/t4b-new-user-faq"
    @message = "Welcome to Our Revolution! If you're new and are looking to get started, simply <#{@document}|follow the steps in our FAQ>.\nIf you have any questions or are stuck, *Text Mentors* are here to help you! Look for users who have the yellow bar on their profile picture - just like mine!\n*Text Mentors are generally available from 9am to 9pm Eastern Time!*"
    Textshifts::App::AssignmentHelper::newbieAnnounce(@message)
  end

  desc 'Posts an alternate message to the welcome channel'
  task :onhold => :environment do |t, args|
    @message = "​*Hi all! We're working on some exciting new updates at the moment, so onboarding is on hold. Hang tight and we'll have announcements as soon as they're ready!*​"
    Textshifts::App::AssignmentHelper::newbieAnnounce(@message)
  end

end

