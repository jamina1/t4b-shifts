source 'https://rubygems.org'

# Padrino supports Ruby version 1.9 and later
ruby '2.2.1'

# Optional JSON codec (faster performance)
gem 'oj'

# Project requirements
gem 'rake'
gem 'dotenv'
gem 'omniauth'
gem 'omniauth-slack'
gem 'httparty'
gem 'tzinfo'
gem 'tzinfo-data'
gem 'sinatra-contrib' #for cookies

# Component requirements
gem 'sass'
gem 'slim'
gem 'redcarpet'
gem 'gravatarify', '~> 3.0.0'
gem 'builder'
gem 'whenever'

# Databases
gem 'sequel'
gem 'sequel_enum'
gem 'mysql2'

group :development do
	# Deployment
	gem 'capistrano'
	gem 'capistrano-bundler'
	gem 'capistrano-rvm'
end

group :test do
	gem 'minitest', :require => 'minitest/autorun'
	gem 'rack-test', :require => 'rack/test'
	gem 'capybara_minitest_spec'
	gem "factory_bot"
	gem "database_cleaner"
	gem 'minitest-reporters'
end

group :development, :test do
	gem 'pry'
	gem 'pry-byebug'
	gem 'pry-stack_explorer'
end

# Padrino Stable Gem
gem 'padrino', '0.13.2'