class User < Sequel::Model
	plugin :timestamps
	plugin :serialization
	serialize_attributes :json, :roles

	one_to_many :assignments
	many_to_many :shifts, :join_table => :assignments

	# Scopes

	subset(:active, :active => true)
	subset(:notify, :notify => true, :active => :true)
	subset(:inactive, :active => false)

	def validate
		super
		validates_presence :email, :message => "Your Email is required!"
		validates_presence :uid, :message => "We couldn't get your user name from Slack!"
		validates_unique :email, :message => "Email is already in use!"
		validates_unique :uid, :message => "That Slack ID is already registered!"
	end

	def is_admin?
		return false unless self.roles
		return self.roles.include?("admin")
	end

	def active?
		return self.active
	end

	def add_roles(*perms)
		current = self.roles || []
		self.roles = current + perms
		self.save
	end

	def remove_roles(*perms)
		current = self.roles || []
		self.roles = current - perms
		self.save
	end

	# Update user information from the most current Omniauth auth hash
	def update_from_auth(auth)
		return false unless @user = User[self.id]
		@user.update(:email => auth[:email], :name => auth[:name], :image => auth[:image])
	end

	# Omniauth Create
	def self.new_from_auth(auth)
		user = User.new
		user.email = auth[:email]
		user.uid = auth[:user]
		user.name = auth[:name] if auth[:name]
		user.image = auth[:image] if auth[:image]
		user.active = true

		user.save
		user
	end

	# Sequel Finders

	def User.by_id(id)
		where(:id => id)
	end

	def User.by_uid(uid)
		where(:uid => uid)
	end

	def User.by_email(email)
		where(:email => email)
	end

	User.finder :by_id
	User.finder :by_uid
	User.finder :by_email

	User.set_allowed_columns(:email, :uid, :name, :phone, :device, :image, :tz, :location, :roles, :active)
end