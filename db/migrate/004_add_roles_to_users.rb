Sequel.migration do
  up do
    alter_table :users do
      add_column :roles, String
    end
  end

  down do
    alter_table :users do
      drop_column :roles
    end
  end
end
