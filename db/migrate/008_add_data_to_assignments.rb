Sequel.migration do
  up do
    alter_table :assignments do
      add_column :assigned_to, String
      add_column :assigned_by, String
    end
  end

  down do
    alter_table :assignments do
      drop_column :assigned_to
      drop_column :assigned_by
    end
  end
end
