Sequel.migration do
  up do
    alter_table :users do
      add_column :phone, String
      add_column :device, String
    end
  end

  down do
    alter_table :users do
      drop_column :phone
      drop_column :device
    end
  end
end
