Sequel.migration do
  up do
    create_table :shifts do
      primary_key :id
      Date :start_date, :index => true, :nil => false
      Date :reminder_date, :index => true
      String :name, :nil => false
      Text :details
      Int :capacity, :max => 1000
      Bool :allow_dupes
      Bool :active, :default => true
      String :event_link

      DateTime :created_at
      DateTime :updated_at
    end
  end

  down do
    drop_table :shifts
  end
end
