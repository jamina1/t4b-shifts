#!/usr/bin/env rackup
# encoding: utf-8

# This file can be used to start Padrino,
# just execute it from the command line.

require File.expand_path("../config/boot.rb", __FILE__)

use Rack::Session::Cookie,
	:key => 'rack.session',
	:expire_after => 3600 * 8,
	:path => '/',
	:domain => ENV['COOKIE_DOMAIN'],
	:secret => ENV['SESSION_SECRET']

use Rack::Protection, :except => :path_traversal
# use Rack::Protection::FormToken, :origin_whitelist => "localhost"

use OmniAuth::Builder do
	provider :slack,
		ENV["SLACK_APP_ID"],
		ENV["SLACK_APP_KEY"],
		scope: ENV["SLACK_SCOPES"],
		team: ENV["SLACK_TEAM_ID"]
		# name: :sign_in_with_slack
	provider :slack,
		ENV['SLACK_APP_ID'],
		ENV['SLACK_APP_KEY'],
		scope: ENV['SLACK_PERMISSIVE_SCOPES'],
		team: ENV['SLACK_TEAM_ID'],
		name: :new_user
end

run Padrino.application