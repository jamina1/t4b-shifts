# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'textshifts'
set :repo_url, 'git@bitbucket.org:jamina1/t4b-shifts.git'

set :deploy_to, '/home/hustlebern'

set :deploy_via, :copy
set :scm, 'git'
set :branch, 'master'
set :git_shallow_clone, 1
set :scm_verbose, true
set :use_sudo, false

set :bundle_dir, ''
set :bundle_without, %w{development test staging}.join(' ')
set :bundle_flags, '--quiet'

set :linked_dirs, %w(log tmp)

set :rvm_type, :user
set :rvm_ruby_version, '2.2.1@textshifts'

set :tmp_dir, "/home/hustlebern/tmp"

before :deploy, "deploy:run_tests"

namespace :deploy do

	desc "Run tests before deploying"
	task :run_tests do
	run_locally do
		with rack_env: "development" do
		unless system "bundle exec rake test"
			puts "--> Tests Failed!!"
			exit;
		end
		puts "--> All Tests passed"
		end
	end
	end

	after :published, :upgrade do
	on roles(:db) do
		within release_path do
		with rack_env: "#{fetch(:stage)}" do
			 execute :rake, "sq:migrate:up"
		end
		end
	end
	end

	after :restart, :restart_passenger do
	on roles(:web), in: :groups, limit: 3, wait: 10 do
		within release_path do
			execute :touch, 'tmp/restart.txt'
		end
	end
	end
	after :finishing, 'deploy:restart_passenger'

end

namespace :env do
	desc "Upload environment file"
	task :upload do
		on roles(:all) do
			upload! ".env.#{fetch(:stage)}", "#{shared_path}"
		end
	end
end

before "deploy:check", "env:upload"
