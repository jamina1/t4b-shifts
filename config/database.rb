Sequel::Model.plugin :schema
Sequel::Model.plugin :timestamps
Sequel::Model.plugin :validation_helpers
Sequel::Model.plugin :json_serializer
Sequel::Model.plugin :force_encoding, 'UTF-8'
Sequel::Model.raise_on_save_failure = false # Do not throw exceptions on failure

default_params = {
  :database => ENV['DATABASE_DB'],
  :host => ENV['DATABASE_HOST'],
  :user => ENV['DATABASE_USER'],
  :password => ENV['DATABASE_PASS'],
  :adapter => 'mysql2',
  :encoding => 'utf8mb4',
  :reconnect => true
}

Sequel::Model.db = case Padrino.env
	# If you would prefer to use sqlite for development, uncomment the following line
	# when :development	then Sequel.connect("sqlite://development.db", :loggers => [logger])
	when :development, :production
		Sequel.connect(default_params, :loggers => [logger])
	when :staging
		Sequel.connect(ENV['DATABASE_URL'], :loggers => [logger])
	when :test
		Sequel.connect(
			default_params.merge(:database => 'hustletest'),
			:loggers => [logger]
		)
end
