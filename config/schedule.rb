# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "log/cron_log.log"

job_type :padrino_rake, 'cd :path && padrino rake :task -e :environment'

# Welcome Newbies!
# every "0 4-22/2 * * *" do
# 	padrino_rake "textforbernie_new:welcome"
# end

# every 5.minutes do
# 	command "curl -Is 'shifts.textforbernie.com'"
# end

every 1.day, :at => '1:59pm' do
	padrino_rake "holler:announce_morning['Eastern']"
end
every 1.day, :at => '2:59pm' do
	padrino_rake "holler:announce_morning['Central']"
end
every 1.day, :at => '3:59pm' do
	padrino_rake "holler:announce_morning['Mountain']"
end
every 1.day, :at => '4:59pm' do
	padrino_rake "holler:announce_morning['Pacific']"
end

every 1.day, :at => '1:59am' do
	padrino_rake "holler:announce_evening['Eastern']"
end
every 1.day, :at => '2:59pm' do
	padrino_rake "holler:announce_evening['Central']"
end
every 1.day, :at => '3:59am' do
	padrino_rake "holler:announce_evening['Mountain']"
end
every 1.day, :at => '4:59am' do
	padrino_rake "holler:announce_evening['Pacific']"
end

# every 30.minutes do
# 	padrino_rake "grumble"
# end

# every 1.day, :at => '9:59am' do
# 	padrino_rake "holler:announce_morning['Alaska']"
# end
# every 1.day, :at => '11:59am' do
# 	padrino_rake "holler:announce_morning['Hawaii']"
# end
