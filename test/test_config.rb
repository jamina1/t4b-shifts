require 'database_cleaner'
require 'capybara'
require 'capybara_minitest_spec'
require 'minitest/reporters'

RACK_ENV = 'test' unless defined?(RACK_ENV)
require File.expand_path(File.dirname(__FILE__) + "/../config/boot")
Dir[File.expand_path(File.dirname(__FILE__) + "/../app/helpers/**/*.rb")].each(&method(:require))

DatabaseCleaner.strategy = :transaction

Capybara.default_max_wait_time = 10

Dotenv.load(".env.#{RACK_ENV}")

reporter_options = { color: true }
Minitest::Reporters.use! [Minitest::Reporters::DefaultReporter.new(reporter_options)]

class MiniTest::Spec
	include Rack::Test::Methods
	include FactoryBot::Syntax::Methods

	FactoryBot.find_definitions

	OmniAuth.config.test_mode = true
	omniauth_hash = OmniAuth::AuthHash.new({
			'provider' => 'slack',
			'info' => {
				'email' => 'test@test.com',
				'name' => 'Elanor Riley',
				'user' => 'oranges13',
				'image' => 'http://placehold.it/192x192'
			},
			'credentials' => {
				'token' => 'mock_token',
				'secret' => 'mock_secret',
			}
		})

	OmniAuth.config.add_mock(:slack, omniauth_hash)

	updated_hash = OmniAuth::AuthHash.new({
			'provider' => 'slack',
			'info' => {
				'email' => 'test2@test.com',
				'name' => 'Elanor Riley',
				'user' => 'oranges13',
				'image' => 'http://placehold.it/192x192'
			},
			'credentials' => {
				'token' => 'mock_token',
				'secret' => 'mock_secret',
			}
		})

	OmniAuth.config.add_mock(:slack_updated, updated_hash)

	before {
		DatabaseCleaner.start
	}

	after {
		DatabaseCleaner.clean
	}


	def app(app = nil, &blk)
		@app ||= block_given? ? app.instance_eval(&blk) : app
		@app ||= Padrino.application
	end

	def session
		last_request.env['rack_session']
	end

	def login_user
		get "/auth/slack/callback", nil, {"omniauth.auth" => OmniAuth.config.mock_auth[:slack]}
		return last_request.env['rack.session']
	end

	def wait_for
		Timeout.timeout(Capybara.default_max_wait_time) do
			loop until
				begin
					yield
				rescue MiniTest::Assertion
			end
		end
		yield
	end
	
end