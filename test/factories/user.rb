FactoryBot.define do

	sequence :email do |n|
		"person#{n}@example.com"
	end

	sequence :uid do |n|
		"user#{n}"
	end

	factory :user do

		uid
		email
		name "Cheese Orange"
		phone "2693503302"
		notify true
		active true

		trait :with_shifts do
			after(:create) do |user|
				shift = FactoryBot.create(:shift)
				FactoryBot.create_list(:assignment, 5, shift: shift, user: user)
			end
		end

		trait :admin do
			roles ["admin"]
		end

	end
end