FactoryBot.define do
	factory :assignment do
		association :user, :factory => :user
		association :shift, :factory => :shift
		status { [:pending, :claimed, :assigned, :cancel, :missing].sample }
	end
end

