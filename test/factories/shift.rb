FactoryBot.define do

	factory :shift do

		sequence(:name) { |n| "BarnStorm ##{n}" }
		capacity Random.rand(50..100)
		start_date {Date.today + 2 }
		details "Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI."

		trait :reminder do
			# reminder true
			reminder_date { Date.today + 5 }
		end

		trait :completed do
			start_date { Date.today - 2 }
		end

		trait :inactive do
			active false
		end

	end
end