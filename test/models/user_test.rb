require File.expand_path(File.dirname(__FILE__) + '/../test_config.rb')

describe User, "User Model" do

	before(:each) do
		@user = build(:user)
	end

	it 'can construct a new instance' do
		refute_nil @user
	end

	describe "roles" do

		before(:each) do
			@user = build(:user)
			@user.roles = ["cheese"]
			@user.save
		end

		it "should accept roles" do
			@user.add_roles("admin")
			@user.roles.must_include "admin"
		end

		it "should unserialize and display roles" do
			@user.add_roles("admin", "IT")

			@user.roles.must_include "admin"
			@user.roles.must_include "IT"
		end

		it "should remove roles" do
			@user.remove_roles("cheese")
			@user.roles.wont_include "cheese"
		end

		it "should indicate admin status" do
			@user.add_roles("admin")
			@user.is_admin?.must_equal true

			@user.remove_roles("admin")
			@user.is_admin?.must_equal false
		end

	end

	describe "validations" do
		it "should require uid" do
			@user.uid = nil
			@user.valid?.wont_equal true
			@user.errors[:uid].must_include "We couldn't get your user name from Slack!"
		end

		it "should require email" do
			@user.email = nil
			@user.valid?.wont_equal true
			@user.errors[:email].must_include "Your Email is required!"
		end

		it "should save valid entry" do
			@user = build(:user)
			@user.save.must_be_instance_of User
		end
	end

end