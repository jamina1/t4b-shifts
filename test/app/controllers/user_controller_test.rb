require File.expand_path(File.dirname(__FILE__) + '/../../test_config.rb')

describe "/user" do

	before do
		@user = create(:user)
		@admin = create(:user, :roles => ["admin"])
	end

	describe "users profile" do
		
		it "should let users view their profile" do
			get "/profile", nil, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.body.must_have_css("h1", :text => @user.uid)
			# last_response.body.must_have_content(@user.name)
		end

		it "should let users edit their profile" do
			get "/profile/edit", nil, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.body.must_have_field("name", :with => @user.name)
			last_response.body.must_have_field("email", :with => @user.email)
			last_response.body.must_have_field("phone")
			last_response.body.must_have_field("location")
			# last_response.body.must_have_select("device")
			last_response.body.must_have_select("tz")
		end

		it "should accept updated values" do
			put "/profile/update", {:phone => "5555555555", :location => "apple"}, {'rack.session' => {:current_user => @user.id, :roles => []}}
			assert_equal User[@user.id].location, "apple"
			assert_equal User[@user.id].phone, "5555555555"
		end

		it "should let users disable their account"
	end
	
	it "should let admins view a list of all users" do
		get "/users", nil, {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}}
		last_response.body.must_have_css("h1", :text => "All Users")
		last_response.body.must_have_css("table.datatable")
	end

	it "should let admins edit user details" do
		get "/users/admin_edit/#{@user.id}", nil, {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
		last_response.body.must_have_field("user[name]", :with => @user.name)
		last_response.body.must_have_field("user[email]", :with => @user.email)
		last_response.body.must_have_field("user[phone]")
		last_response.body.must_have_field("user[location]")
		# last_response.body.must_have_select("user[device]")
		last_response.body.must_have_field("user[tz]")
	end

	it "should let admins update users" do
		post "/users/admin_update/#{@user.id}", {:user => {:email => "cheese@cheese.com"}}, {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
		assert_equal User[@user.id].email, "cheese@cheese.com"
	end

	it "should let admins deactivate users" do
		post "/users/activate/#{User.first.id}", nil, {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
		assert_equal User.first.active, false
	end

	it "should let admins activate users" do
		User.first.update(:active => false)
		post "/users/activate/#{User.first.id}", nil, {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
		assert_equal User.first.active, true
	end

	it "should let admins add roles" do
	end

end
