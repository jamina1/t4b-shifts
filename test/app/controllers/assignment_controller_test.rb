require File.expand_path(File.dirname(__FILE__) + '/../../test_config.rb')

describe "/assignment" do

	before do 
		@user = create(:user, :with_shifts)
		@admin = create(:user, :admin)
		@assignment = Assignment.last
	end

  it "should let user get list of signups" do
    post "/shift/#{Shift.last.id}/assignment/list", {:type => 1}, {'rack.session' => {:current_user => @user.id, :roles => []}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
    json_body = Oj.load(last_response.body)
    assert_equal json_body.count, Assignment.where(:shift_id => Shift.last.id).distinct(:user_id).count
  end

  describe "should let admins update signup status" do
  	it "claim" do
  		post "/assignment/update", {:ids => [@assignment.id], :status => "claimed"} , {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
  		assert_equal Assignment.last.status, :claimed
  	end

  	it "assign" do
  		post "/assignment/confirm", {:ids => [@assignment.id], :status => "assigned", :assigned_to => "test"} , {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
  		assert_equal Assignment.last.status, :assigned
  		assert_equal Assignment.last.assigned_to, "test"
  	end

  	it "missing" do
  		post "/assignment/update", {:ids => [@assignment.id], :status => "missing"} , {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
  		assert_equal Assignment.last.status, :missing
  	end

  	it "cancel" do
  		post "/assignment/update", {:ids => [@assignment.id], :status => "cancel"} , {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
  		assert_equal Assignment.last.status, :cancel
  	end

  	it "reset" do
  		post "/assignment/update", {:ids => [@assignment.id], :status => "pending"} , {'rack.session' => {:current_user => @admin.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
  		assert_equal Assignment.last.status, :pending
      assert_equal Assignment.last.assigned_to, nil
  	end
  end
  
  it "should send a notification to the user when they've been assigned"
end
