require File.expand_path(File.dirname(__FILE__) + '/../../test_config.rb')

describe "/auth" do
	
	describe "GET :logout" do
		before do
			get_logout
		end
		
		it "empty the current session" do
			session.must_equal nil
		end

		it "redirect to homepage if user is logging out" do
			assert last_response.status, 301
		end
	end

	describe "logging in with omniauth" do
		it "should login with valid user" do
			get "/auth/slack/callback", nil, {"omniauth.auth" => OmniAuth.config.mock_auth[:slack]}
			assert last_response.status, 301
			assert User.count, 1
		end

		it "should update user information when logging in" do
			User.first.destroy
			@user = User.new_from_auth(OmniAuth.config.mock_auth[:slack][:info])
			assert @user.instance_of?(User), true
			get "/auth/slack/callback", nil, {"omniauth.auth" => OmniAuth.config.mock_auth[:slack_updated]}
			assert User[@user.id].email, "test2@test.com"
		end

	end

	private
	
	def get_logout
		get '/logout', {:name => 'Hans', :password => "test1234" }, {'rack.session' => {:current_user => 1}}
	end

end
